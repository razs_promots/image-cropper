"use strict";
var cropperConfig = require('./cropperConfig');
var express = require('express');
var app = express();
var http = require('http');
//var fs = require('fs');
const fs = require('fs-extra');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
var cloudinary = require("cloudinary");
var wget = require('node-wget');
var archiver = require('archiver');
var async = require('async');

const imageDir = cropperConfig.serviceFolder+'public/downloads/images';
const animativiDir = cropperConfig.serviceFolder+'public/downloads/animativi';
const reportsDir = cropperConfig.serviceFolder+'public/downloads/reports';

const winston = require('winston');

const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({ 
        timestamp: tsFormat,
        colorize: true,
        level: 'info'
    }),
    new (require('winston-daily-rotate-file'))({
      filename: cropperConfig.serviceFolder+'logs/%DATE%_cropperLog.log',
      timestamp: tsFormat,
      maxFiles: '7d',
      level: env === 'development' ? 'verbose' : 'info'
    })  
  ]
});

var logDate = new Date();
logger.info('Proper Cropper Log');
logger.info(logDate);

app.use(express.static("."));

app.get('/', function(req, res) {
    res.sendFile(__dirname + "/" + "index.html");
});

// set-up cloudinary config for creating the zip file containing the images, as well as deleting/creating images in cloudinary
cloudinary.config({ 
  cloud_name: 'dmkk8ozw6', 
  api_key: '395265336375895', 
  api_secret: 'Wx5U-ydAGsmET1qcahJwi3jo80c' 
});

//after files have been uploaded to cloudinary via the widget - create zip file containing the images and return it's url
app.get('/upload', function(req, res){
    
    // first delete all zips from server
    var downloads = (cropperConfig.serviceFolder+'public/downloads/zips');
    fs.readdir(downloads, (err, files) => {
        if (err) throw err;
        for (const file of files) {
            fs.unlink(path.join(downloads, file), err => {
                if (err) throw err;
            });    
        }
    });
    
    //response gets the input from the user & functions both as the tag for the images and the name of the zip file
    var response = req.query.name
    
    // get response for approved and failed images (Full_Frames), and omit the last character (~)
    var approvedImages = req.query.approved.slice(0,-1);
    var failedImages = req.query.failed.slice(0,-1);
    
    // get response for flipped images (Full_Frames), both URL & image name, and omit the last character (~)
    var flippedImages = req.query.flip.slice(0,-1);
    var flippedNames = req.query.flipNames.slice(0,-1);
    
    // get response for animativi (boolean), and also names & URLs of approved ones (animativi) - omit the last character (~)
    var animativi = req.query.animativi
    var animApproved = req.query.animApproved.slice(0,-1);
    var animativiURL = req.query.animURL.slice(0,-1);
    
    var removeExt = req.query.removeExt
    
    //split strings into arrays - "~," acts as divider
    var approved = approvedImages.split("~,");
    var flipArray = flippedImages.split("~,");
    var flipNamesArray = flippedNames.split("~,");
    var animImages = animApproved.split("~,");
    var animURL = animativiURL.split("~,");
    var failed = failedImages.split("~,");
    
    // counters and lengths to go through the arrays until the end
    var count = 0;
    var anim_count = 0;
    var flip_count = 0;
    
    var failed_length = Object.keys(failed).length;
    var flipslength = Object.keys(flipArray).length;
    var animativi_length = Object.keys(animImages).length;
    
    // variable that will contain the URLS of the zip(s) files to send back to the web service
    var urls = "";
    var d = new Date();
    
     if (flipNamesArray[0] == "") {
        flipslength --;
    }
    
    if (failed[0] == "") {
        failed_length --;
    }
    
    if (animImages[0] == "") {
        animativi_length --;
    }

    var overallApproved = Object.keys(approved).length + flipslength
    
//     log images required process
    logger.info("\n Cropping Name: " + response + "\n Date: " + d + "\n")
    
    logger.info("Approved images: " + approved + "," + flipNamesArray + "\n Overall Approved images: " + overallApproved + "\n")
    logger.info("-------------------------------------------\n")
    
    logger.info("Unapproved images: " + failed + "\n Overall Failed images: " + failed_length + "\n")
    logger.info("-------------------------------------------")
    
    logger.info("Flipped images: " + flipNamesArray + "\n Overall Flipped images: " + flipslength + "\n")
    logger.info("-------------------------------------------\n")
    
    logger.info("Animativi images: " + animImages + "\n Overall Animativi images: " + animativi_length + "\n")
    logger.info("-------------------------------------------\n")
    
    var failedList = failed.join('\n');
    
    var report = "Cropping Name:," + response + "\n \n Date:," + d + "\n \n Unapproved images: \n" + failedList + "\n \n Overall Failed images:," + failed_length
    
    // write unapproved images to file, later will be downloaded by the user along with the zip
    if (failed_length > 0) {
        fs.writeFile('./downloads/reports/'+response+'_report.csv', report, (err) => {
        if (err) throw err;
            console.log("Report file was succesfully saved!");
        });     
    }
   
    
    // for each apporved animativi image, upload the image with required transformation with the suffix "_animativi" & in a folder
    animURL.forEach(function(anim_image,index) {
        cloudinary.v2.uploader.upload(anim_image, { 
            crop: 'fill',
            gravity: 'faces',
            height: 300,
            width: 450,
            public_id: animImages[index]+"_animativi",
            flatten_folders: true,
//            folder: 'animativi_' + response, 
            tags: [response]
        }, function(resultAnimativi) {
            anim_count ++;
            // when done with animativi then run through all the not apporved Full frame images and delete it from cloudinary
            if (anim_count >= animativi_length) {
                failed.forEach(function(failed_image,f_index) {
                    cloudinary.v2.uploader.destroy(failed_image, function(error,resultUnapproved) {
                    count ++;
                    // when done with failed images then run through all the flipped images, upload it with the flip transformation, but replace the original (by using the same public_ID)    
                    if (count >= failed_length) {
                        flipArray.forEach(function(image,index) {
                            cloudinary.v2.uploader.upload(image, { 
                            transformation: {angle: 'hflip'},
                            public_id: flipNamesArray[index],
                            tags: [response]
                            }, function(resultFlip) {
                                flip_count ++;
                                // when done with flipped images then create the zip by using the cloudinary tag (response variable)
                                if (flip_count >= flipslength) {
                                    
//                                    deprecated script - used to create zip through cloudinary - cancelled because of file size limit
//                                    cloudinary.v2.uploader.create_zip(
//                                    { tags: response, resource_type: 'image', target_public_id: response+'.zip' },
//                                    function(error,resultZip) {
////                                        urls = result.url;
//                                        logger.info(resultZip);
//                                        logger.error(error);
//                                        res.send(resultZip.url);
//                                    });
                                
                                // get an array of all images with the requested tag    
                                cloudinary.v2.api.resources_by_tag(response, {max_results: 500}, function(error, resultArray){
                                        var imagesArray = [];
                                        var dir = (cropperConfig.serviceFolder+'public/downloads/');
                                    
                                        // create a file to stream archive data to.
                                        var fileOutput = fs.createWriteStream('./downloads/zips/'+response+'.zip');
                                        var archive = archiver('zip', {
                                        zlib: { level: 0 } // Sets the compression level.
                                        });
                                        logger.info("number of images to download: " + resultArray.resources.length)
                                    
                                        async.each(resultArray.resources,
                                        function(item, callback){
                                            var destination = "";
                                            if (item.public_id.slice(-9) != 'animativi') {
                                                destination = dir+"images/";
                                            } else {
                                                destination = dir+"animativi/";
                                            }
                                            wget({
                                                url: item.url, 
                                                dest: destination
                                            }, function (error, response, body) {                        
                                                var path1 = dir+"images/"+item.public_id+"."+item.format
                                                var path2 = dir+"images/"+item.public_id
                                                
                                                if (item.public_id.slice(-9) != 'animativi' && removeExt == 'true') {
                                                    fs.rename(path1, path2);
                                                }
                                                callback();
                                            });
                                        },
                                               
                                        function(err){
                                        // All tasks are done now
                                        logger.info("finished downloading images from cloudinary");
                    
                                        var fileName =  response+'.zip'
                                        archive.pipe(fileOutput);
                                                
                                        fileOutput.on('close', function () {
                                            logger.info(archive.pointer() + ' total bytes');
                                            logger.info('archiver has been finalized and the output file descriptor has closed.');
                                            res.send(fileName);
                                        });
                                        archive.directory(dir+"images", "FullFrame");
                                        archive.directory(dir+"animativi", "Animativi");
                                        archive.directory(dir+"reports", false);
                                        
                                        // add as many as you like
                                        archive.on('error', function(err){
                                            throw err;
                                        });
                                        archive.finalize();   
                                        }
                                    );
                                    
                                    
                                });    
                                
                                }       
                            });
                        });       
                    }
                    });
                });        
            }
        });  
    });
});

// After zip has been downloaded delete all images with the tag from cloudinary
app.get('/done', function(req, res){
    var response = req.query.name
    
    cloudinary.v2.api.delete_resources_by_tag(response,
    function(error, result){
//        console.log(result);
        logger.info("Cloudinary response (delete images): "); 
        logger.info(result);
        res.send(result)
    });
    
    
    fs.readdir(imageDir, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            fs.unlink(path.join(imageDir, file), err => {
                if (err) throw err;
            });
        }
    });
    
    fs.readdir(animativiDir, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            fs.unlink(path.join(animativiDir, file), err => {
                if (err) throw err;
            });
        }
    });
    
    fs.readdir(reportsDir, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            fs.unlink(path.join(reportsDir, file), err => {
                if (err) throw err;
            });
        }
    });
    
    
});

//server creation
var server = app.listen(cropperConfig.restPort, function(){
        var host = server.address().address;
        var port = server.address().port;
        logger.info("Started Proper Cropper service");
        
    });
